document.addEventListener('DOMContentLoaded', function() {
    var searchInput = document.querySelector(".input-field");
    var searchResults = document.getElementById('searchResults');
    var footer = document.querySelector('.footer');
    var clearButton = document.querySelector('.fa-solid.fa-chevron-up');
    var count = 0; 
    var itemsLeft = footer.querySelector('li:first-child');

    searchInput.addEventListener('focus', function() {
        this.style.border = "1px solid rgb(217, 161, 161)";
    });

    searchInput.addEventListener('blur', function() {
        this.style.border = "none";
    });

    footer.style.display = 'none';
    clearButton.style.display="none"

    clearButton.addEventListener('click', function() {
        searchInput.value = ''; 
        searchResults.querySelectorAll('input[type="checkbox"]').forEach(function(checkbox) {
            checkbox.checked = !checkbox.checked; // Toggle the checked state
            var textNode = checkbox.parentNode.querySelector('span');
            if (checkbox.checked) {
                textNode.style.textDecoration = 'line-through';
                count--;
            } else {
                textNode.style.textDecoration = 'none';
                count++;
            }
        });
        updateItemsLeft(); 
    });

    searchInput.addEventListener('keydown', function(event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            var searchTerm = searchInput.value.trim();
            if (searchTerm !== '') {
                footer.style.display = 'block';
                clearButton.style.display="block"
                var resultItem = document.createElement('div');
                resultItem.style.width = '703.5px';
                resultItem.style.height = '60px';
                resultItem.style.borderBottom = '1px solid black';
                resultItem.style.background = 'white';
                resultItem.style.position = 'relative';
                resultItem.style.display = 'flex';
                resultItem.style.alignItems = 'center';
                resultItem.style.fontSize = '25px';
                resultItem.style.order = '1';
                resultItem.style.boxShadow = "rgba(0, 0, 0, 0.1) 0px 0px 5px 0px, rgba(0, 0, 0, 0.1) 0px 0px 1px 0px;"

                var checkbox = document.createElement('input');
                checkbox.type = 'checkbox';
                checkbox.style.width = '30px';
                checkbox.style.height = '30px';
                checkbox.style.marginRight = '20px';
                checkbox.style.marginLeft = '20px';

                checkbox.addEventListener('change', function() {
                    var textNode = resultItem.querySelector('span'); 
                    if (this.checked) {
                        textNode.style.textDecoration='line-through';
                        count--;
                    } else {
                        textNode.style.textDecoration='none';
                        count++;
                    }
                    updateItemsLeft(); 
                });

                resultItem.appendChild(checkbox);

                var textNode = document.createElement('span'); 
                textNode.textContent = searchTerm;
                textNode.addEventListener('dblclick', function() {
                    var inputField = document.createElement('input');
                    inputField.type = 'text';
                    inputField.value = textNode.textContent;
                    inputField.style.fontSize = '25px';
                    inputField.style.width = 'calc(100% - 50px)';
                    inputField.style.height = '100%';
                    inputField.style.border = 'none';
                    inputField.style.outline = 'none';
                    inputField.style.background = 'transparent';
                    inputField.style.textAlign = 'left';
                    inputField.style.padding = '0';
                    inputField.style.margin = '0';
                    inputField.style.color = '#000';
                    inputField.style.overflow = 'hidden';
                    inputField.addEventListener('keydown', function(event) {
                        if (event.key === 'Enter') {
                            event.preventDefault();
                            textNode.textContent = inputField.value.trim();
                            inputField.replaceWith(textNode);
                        }
                    });
                    resultItem.replaceChild(inputField, textNode);
                    inputField.focus();
                });

                resultItem.appendChild(textNode);

                searchResults.appendChild(resultItem);
                
                searchInput.value = '';
                count++;
                updateItemsLeft(); 
            }
        }
    });

    function updateItemsLeft() {
        itemsLeft.textContent = count + " item(s) left";
    }

    footer.querySelectorAll('button').forEach(function(button) {
        button.addEventListener('click', function() {
            var filter = this.textContent.toLowerCase();
            searchResults.querySelectorAll('div').forEach(function(resultItem) {
                var checkbox = resultItem.querySelector('input[type="checkbox"]');
                if (filter === 'all') {
                    resultItem.style.display = 'flex';
                } else if (filter === 'active' && checkbox.checked) {
                    resultItem.style.display = 'none';
                } else if (filter === 'completed' && !checkbox.checked) {
                    resultItem.style.display = 'none';
                } else {
                    resultItem.style.display = 'flex';
                }
            }); 
        }); 
    });

    footer.querySelector('a').addEventListener('click', function() {
        searchResults.querySelectorAll('div').forEach(function(resultItem) {
            var checkbox = resultItem.querySelector('input[type="checkbox"]');
            if (checkbox.checked) {
                resultItem.remove();
                count--;
            }
        });
        count = Math.max(count, 0);
        updateItemsLeft(); 
    });
});
